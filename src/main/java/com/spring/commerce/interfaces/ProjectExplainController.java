package com.spring.commerce.interfaces;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author hwang-yunho on 2020. 4. 15.
 * @project commerce
 */
@Controller
public class ProjectExplainController {

    @GetMapping("/")
    public String aboutProject() {
        return "main";
    }
}
