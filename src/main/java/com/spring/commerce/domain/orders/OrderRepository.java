package com.spring.commerce.domain.orders;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author hwang-yunho on 2020. 2. 4.
 * @project commerce
 */
public interface OrderRepository extends JpaRepository<Orders, Long> {
    Orders save(Orders order);
}